defmodule Diesel.Repo do
  use Ecto.Repo,
    otp_app: :diesel,
    adapter: Ecto.Adapters.Postgres
end
