defmodule Diesel.Blockchain.Block do
    @moduledoc """
    The Blockchain.Block Model
    """
  use Ecto.Schema
  import Ecto.Changeset
  import Diesel.Blockchain.Validations



#   @allowed_types Enums.keys(Enums.Block.Type)
  @fields_required [:type, :data, :prev, :hash, :creator, :signature, :timestamp]
#   @derive {Poison.Encoder, only: [:id, :meta | @fields_required]}

  schema "blocks" do
    field :creator, :string
    field :data,      :map, default: %{}
    field :hash, :string
    field :prev, :string
    field :signature, :string
    # field :type,      Enums.Block.Type
    field :timestamp, :naive_datetime

    timestamps()
  end

  @doc false
  def changeset(block, attrs) do
    block
    |> cast(attrs, [:type, :prev, :hash, :signature, :creator, :data])
    |> validate_required([:type, :prev, :hash, :signature, :creator, :data])
  end
end
