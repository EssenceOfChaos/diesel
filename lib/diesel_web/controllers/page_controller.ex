defmodule DieselWeb.PageController do
  use DieselWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
