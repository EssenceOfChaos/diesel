defmodule DieselWeb.BlockController do
  use DieselWeb, :controller

  alias Diesel.Blockchain
  alias Diesel.Blockchain.Block

  def index(conn, _params) do
    blocks = Blockchain.list_blocks()
    render(conn, "index.html", blocks: blocks)
  end

  def new(conn, _params) do
    changeset = Blockchain.change_block(%Block{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"block" => block_params}) do
    case Blockchain.create_block(block_params) do
      {:ok, block} ->
        conn
        |> put_flash(:info, "Block created successfully.")
        |> redirect(to: Routes.block_path(conn, :show, block))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    block = Blockchain.get_block!(id)
    render(conn, "show.html", block: block)
  end

  def edit(conn, %{"id" => id}) do
    block = Blockchain.get_block!(id)
    changeset = Blockchain.change_block(block)
    render(conn, "edit.html", block: block, changeset: changeset)
  end

  def update(conn, %{"id" => id, "block" => block_params}) do
    block = Blockchain.get_block!(id)

    case Blockchain.update_block(block, block_params) do
      {:ok, block} ->
        conn
        |> put_flash(:info, "Block updated successfully.")
        |> redirect(to: Routes.block_path(conn, :show, block))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", block: block, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    block = Blockchain.get_block!(id)
    {:ok, _block} = Blockchain.delete_block(block)

    conn
    |> put_flash(:info, "Block deleted successfully.")
    |> redirect(to: Routes.block_path(conn, :index))
  end
end
