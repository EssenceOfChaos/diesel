defmodule DieselWeb.Router do
  use DieselWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", DieselWeb do
    pipe_through :browser

    get "/", PageController, :index
    resources "/blocks", BlockController
  end

  # Other scopes may use custom stacks.
  # scope "/api", DieselWeb do
  #   pipe_through :api
  # end
end
