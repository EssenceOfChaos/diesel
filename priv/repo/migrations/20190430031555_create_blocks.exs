defmodule Diesel.Repo.Migrations.CreateBlocks do
  use Ecto.Migration

  def change do
    create table(:blocks) do
      add :timestamp, :naive_datetime, null: false
      add :type, :integer,      null: false
      add :prev, :string,       null: false
      add :hash, :string,       null: false
      add :signature, :text,    null: false
      add :creator, :text,      null: false
      add :data, :map,          null: false, default: "{}"

      timestamps()
    end

    create index(:blocks, [:hash])
    create index(:blocks, [:prev])
    create index(:blocks, [:creator])
  end
end
