defmodule Diesel.BlockchainTest do
  use Diesel.DataCase

  alias Diesel.Blockchain

  describe "blocks" do
    alias Diesel.Blockchain.Block

    @valid_attrs %{creator: "some creator", data: %{}, hash: "some hash", prev: "some prev", signature: "some signature", type: 42}
    @update_attrs %{creator: "some updated creator", data: %{}, hash: "some updated hash", prev: "some updated prev", signature: "some updated signature", type: 43}
    @invalid_attrs %{creator: nil, data: nil, hash: nil, prev: nil, signature: nil, type: nil}

    def block_fixture(attrs \\ %{}) do
      {:ok, block} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Blockchain.create_block()

      block
    end

    test "list_blocks/0 returns all blocks" do
      block = block_fixture()
      assert Blockchain.list_blocks() == [block]
    end

    test "get_block!/1 returns the block with given id" do
      block = block_fixture()
      assert Blockchain.get_block!(block.id) == block
    end

    test "create_block/1 with valid data creates a block" do
      assert {:ok, %Block{} = block} = Blockchain.create_block(@valid_attrs)
      assert block.creator == "some creator"
      assert block.data == %{}
      assert block.hash == "some hash"
      assert block.prev == "some prev"
      assert block.signature == "some signature"
      assert block.type == 42
    end

    test "create_block/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Blockchain.create_block(@invalid_attrs)
    end

    test "update_block/2 with valid data updates the block" do
      block = block_fixture()
      assert {:ok, %Block{} = block} = Blockchain.update_block(block, @update_attrs)
      assert block.creator == "some updated creator"
      assert block.data == %{}
      assert block.hash == "some updated hash"
      assert block.prev == "some updated prev"
      assert block.signature == "some updated signature"
      assert block.type == 43
    end

    test "update_block/2 with invalid data returns error changeset" do
      block = block_fixture()
      assert {:error, %Ecto.Changeset{}} = Blockchain.update_block(block, @invalid_attrs)
      assert block == Blockchain.get_block!(block.id)
    end

    test "delete_block/1 deletes the block" do
      block = block_fixture()
      assert {:ok, %Block{}} = Blockchain.delete_block(block)
      assert_raise Ecto.NoResultsError, fn -> Blockchain.get_block!(block.id) end
    end

    test "change_block/1 returns a block changeset" do
      block = block_fixture()
      assert %Ecto.Changeset{} = Blockchain.change_block(block)
    end
  end
end
