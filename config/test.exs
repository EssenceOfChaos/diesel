use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :diesel, DieselWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :diesel, Diesel.Repo,
  username: "postgres",
  password: "postgres",
  database: "diesel_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
