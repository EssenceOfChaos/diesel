# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :diesel,
  ecto_repos: [Diesel.Repo]

# Configures the endpoint
config :diesel, DieselWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "gG/LxVS2h7dF8/vO66O4cwq5nNMGO2HSBbsgS/LQa57qMkQjOHnuETpNuf9w+gcU",
  render_errors: [view: DieselWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Diesel.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
